# coding=utf-8
from weblocator.qa_test_locator import QATestLocator

class LoginRegisterPage:
    """
    1. Login page elements, such as form, button or option can be selectable
    2. Login page actions, such as login by register user
    3. Register page element and actions (not in this task)
    """
    header_toDoApp = "//a[@class='navbar-brand']"
    header_home = "//a[text()='Home']"
    header_my_tasks = "//a[text()='My Tasks']"
    header_sign_in = "//a[text()='Sign In']"
    header_register = "/a[text()='Register']"

    email_form = "//input[@id='user_email']"
    password_form = "//input[@id='user_password']"
    submit_button = "//input[@type='submit']"
    remember_me_button = "//input[@id='user_remember_me’]"

    sign_in_alert = "//div[@class='alert alert-info']"
    welcome_alert = "//ul[@class='nav navbar-nav navbar-right']/li[1]/a"

    def login(self, driver):
        driver.get(QATestLocator.qaUri)
        driver.find_element_by_xpath(LoginRegisterPage.header_sign_in).click()

        username = driver.find_element_by_xpath(LoginRegisterPage.email_form)
        pswd = driver.find_element_by_xpath(LoginRegisterPage.password_form)

        username.send_keys("haocui1888@gmail.com")
        pswd.send_keys("AeT+&-p>4QfAWSXk")

        driver.find_element_by_xpath(LoginRegisterPage.submit_button).click()
        sign_in_msg = driver.find_element_by_xpath(LoginRegisterPage.sign_in_alert)

        return sign_in_msg.text





