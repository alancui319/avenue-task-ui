

import fauxfactory
import random

class GeneralUtility:


    def alpha_generator(self, size=8, utf8=False):
        """
        Generates a random character string (Letters + Digits)
        Default length of string generated is 8
        Only considers ascii letters and digits
        """
        result = ""
        if utf8:
            alphasize = random.randint(1, size-1)
            utfsize = size - alphasize
            result = fauxfactory.gen_alpha(alphasize) + fauxfactory.gen_utf8(utfsize)
        else:
            result = fauxfactory.gen_alpha(size)
        return result

    def id_generator(self, size=8, utf8=False):
        """
        Generates a random character string (Letters + Digits)
        Default length of string generated is 8
        Only considers ascii letters and digits
        """
        result = ""
        if utf8:
            alphasize = random.randint(1, size-1)
            utfsize = size - alphasize
            result = fauxfactory.gen_alphanumeric(alphasize) + fauxfactory.gen_utf8(utfsize)
        else:
            result = fauxfactory.gen_alphanumeric(size)
        return result

    def num_size_generator(self, size=8, utf8=False):
        """
        Generates a random character string (Letters + Digits)
        Default length of string generated is 8
        Only considers ascii letters and digits
        """
        result = ""
        if utf8:
            alphasize = random.randint(1, size-1)
            utfsize = size - alphasize
            result = fauxfactory.gen_alphanumeric(alphasize) + fauxfactory.gen_utf8(utfsize)
        else:
            result = fauxfactory.gen_numeric_string(size)
        return result

    def num_generator(self, min_value=0, limit=1048576):
        """
        Generates a random number between 0 and limit
        Default limit is 1048578 (Mega number)
        """
        result = fauxfactory.gen_integer(min_value=min_value, max_value=limit)
        return result