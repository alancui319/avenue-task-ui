import re
import datetime
import time

class StringUtils:

    def welcomeUser(self, s):
        start='Welcome, '
        end='!'
        ans = ((s.split(start))[1].split(end)[0])
        return ans

    def getNumber(self, subTaskStr):
        #s = '(1) jack'
        ans = subTaskStr.split('(')[1].split(')')[0]
        return ans

    def getTitle(self, taskTitleStr):
        ans = taskTitleStr.split("'s")[0]
        return ans

    def is_number(self, s):
        try:
            float(s)
            return True
        except ValueError:
            pass

        try:
            import unicodedata
            unicodedata.numeric(s)
            return True
        except (TypeError, ValueError):
            pass

        return False

    def is_specail_chars(self, s):
        if re.match(r'^[_\W]+$(<)>!@#%^&*-{', s):
            return False
        else:
            return True

    def is_white_spaces(self, s):
        if re.match(r'   ', s):
            return False
        else:
            return True

    def validateSubTaskDueDate(self, date_text):

        minyear = 2010
        maxyear = datetime.date.today().year
        mindate = 1
        maxdate = 31
        minmonth = 1
        maxmonth = 12

        dateparts = date_text.split('/')
        try:

            if date_text != time.strptime(date_text, '%d/%m/%Y'):
                raise ValueError("Time format should be DD/MM/YYYY")
            if len(dateparts) != 3:
                raise ValueError("Invalid date format")
            if int(dateparts[2]) > maxyear or int(dateparts[2]) < minyear:
                raise ValueError("Year out of range")
            if int(dateparts[0]) > maxdate or int(dateparts[0]) < mindate:
                raise ValueError("Date out of range")
            if int(dateparts[1]) > maxmonth or int(dateparts[1]) < minmonth:
                raise ValueError("Date out of range")
        except:
                return "pass"
