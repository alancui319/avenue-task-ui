# coding=utf-8

from pageobject.create_manage_task_page import CreateManageTaskPage
from pageobject.login_register_page import LoginRegisterPage
from webutility.stringutils import StringUtils
from webutility.fileutils import GeneralUtility
import datetime


class CreateManageSubTaskPage:
    """
    1. SubTask page elements, such as form, button, option can be selectable
    2. SubTask page actions
    """
    todo_task_name = "//textarea[@id='edit_task']"
    todo_task_id = "//h3"
    subtask_description = "//input[@id='new_sub_task']"
    subtask_due_date = "//input[@id='dueDate']"
    subtask_add_button = "//button[@id='add-subtask']"
    subtask_close_button = "//div[@class='modal-footer ng-scope']/button"
    subtask_latest_list = "//div[@ng-show='task.sub_tasks.length']//tbody/tr[1]/td[2]/a"

    gu = GeneralUtility()

    def inputValidSubTask(self, driver):
        new_task = CreateManageTaskPage()

        if new_task.createTaskName(driver=driver, task_name_str="Create main task" + self.gu.alpha_generator(size=3, utf8=False)) == "pass":
            driver.find_element_by_xpath(CreateManageTaskPage.manage_subtask_button).click()

            subTask_desc = driver.find_element_by_xpath(CreateManageSubTaskPage.subtask_description)
            subTask_due_date = driver.find_element_by_xpath(CreateManageSubTaskPage.subtask_due_date)
            subTask_btn = driver.find_element_by_xpath(CreateManageSubTaskPage.subtask_add_button)

            subTask_desc.send_keys("Create a test plan")
            subTask_due_date.send_keys("20/6/2016")
            subTask_btn.click()

            subTask_top_list_str = driver.find_element_by_xpath(CreateManageSubTaskPage.subtask_latest_list).text
            if subTask_top_list_str == "Create a test plan":
                return "pass"
            else:
                return "fail"

    def emptySubTaskDescription(self, driver):
        new_task = CreateManageTaskPage()

        if new_task.createTaskName(driver=driver, task_name_str="Create main task" + self.gu.alpha_generator(size=3, utf8=False)) == "pass":
            driver.find_element_by_xpath(CreateManageTaskPage.manage_subtask_button).click()

            subTask_desc = driver.find_element_by_xpath(CreateManageSubTaskPage.subtask_description)
            subTask_due_date = driver.find_element_by_xpath(CreateManageSubTaskPage.subtask_due_date)
            subTask_btn = driver.find_element_by_xpath(CreateManageSubTaskPage.subtask_add_button)

            subTask_desc.send_keys("")
            subTask_due_date.send_keys("20/6/2016")
            subTask_btn.click()

            subTask_top_list_str = driver.find_element_by_xpath(CreateManageSubTaskPage.subtask_latest_list).text
            if subTask_top_list_str == "":
                return "pass"
            else:
                return "fail"


    def emptySubTaskDueDate(self, driver):
        new_task = CreateManageTaskPage()

        if new_task.createTaskName(driver=driver, task_name_str="Create main task" + self.gu.alpha_generator(size=3, utf8=False)) == "pass":
            driver.find_element_by_xpath(CreateManageTaskPage.manage_subtask_button).click()

            subTask_desc = driver.find_element_by_xpath(CreateManageSubTaskPage.subtask_description)
            subTask_due_date = driver.find_element_by_xpath(CreateManageSubTaskPage.subtask_due_date)
            subTask_btn = driver.find_element_by_xpath(CreateManageSubTaskPage.subtask_add_button)

            subTask_desc.send_keys("Design a sheet")
            subTask_due_date.send_keys("")
            subTask_btn.click()

            subTask_top_list_str = driver.find_element_by_xpath(CreateManageSubTaskPage.subtask_latest_list).text
            if subTask_top_list_str == "Design a sheet":
                return "pass"
            else:
                return "fail"


    def validateSubTaskDesc(self, driver, input_task_name):
        new_task = CreateManageTaskPage()
        su = StringUtils()

        if new_task.createTaskName(driver=driver, task_name_str="Create main task" + self.gu.alpha_generator(size=3, utf8=False)) == "pass":
            driver.find_element_by_xpath(CreateManageTaskPage.manage_subtask_button).click()

            subTask_desc_form = driver.find_element_by_xpath(CreateManageSubTaskPage.subtask_description)
            subTask_due_date = driver.find_element_by_xpath(CreateManageSubTaskPage.subtask_due_date)
            add_subTask_button = driver.find_element_by_xpath(CreateManageSubTaskPage.subtask_add_button)

            subTask_desc_form.send_keys(input_task_name)
            subTask_due_date.send_keys("28/6/2016")
            add_subTask_button.click()

            if len(input_task_name) > 250:
                return "fail"
            elif len(input_task_name) == 0:
                return "fail"
            elif su.is_number(s=input_task_name):
                return "fail"
            elif su.is_specail_chars(s=input_task_name):
                return "fail"
            elif su.is_white_spaces(s=input_task_name):
                return "fail"
            else:
                return "pass"


    def validateSubTaskDueDate(self, driver, input_due_date):
        new_task = CreateManageTaskPage()
        gu = GeneralUtility()
        su = StringUtils()

        if new_task.createTaskName(driver=driver, task_name_str="Create main task" + self.gu.alpha_generator(size=3, utf8=False)) == "pass":
            driver.find_element_by_xpath(CreateManageTaskPage.manage_subtask_button).click()

            subTask_desc_form = driver.find_element_by_xpath(CreateManageSubTaskPage.subtask_description)
            subTask_due_date = driver.find_element_by_xpath(CreateManageSubTaskPage.subtask_due_date)
            add_subTask_button = driver.find_element_by_xpath(CreateManageSubTaskPage.subtask_add_button)

            subTask_desc_form.send_keys("Testing " + gu.alpha_generator(size=2, utf8=False))
            subTask_due_date.send_keys(input_due_date)
            add_subTask_button.click()

            if su.validateSubTaskDueDate(date_text=input_due_date) == True:
                return "pass"
            else:
                return "fali"


    def validateTaskToDoDescReadOnly(self, driver):
        new_task = CreateManageTaskPage()

        if new_task.createTaskName(driver=driver, task_name_str="Create main task" + self.gu.alpha_generator(size=3, utf8=False)) == "pass":
            driver.find_element_by_xpath(CreateManageTaskPage.manage_subtask_button).click()

            task_todo_desc = driver.find_element_by_xpath(CreateManageSubTaskPage.todo_task_name)
            if task_todo_desc.is_enabled():
                return "fail"
            else:
                return "pass"


    def validateTaskToDoIdReadOnly(self, driver):
        new_task = CreateManageTaskPage()

        if new_task.createTaskName(driver=driver, task_name_str="Create main task" + self.gu.alpha_generator(size=3, utf8=False)) == "pass":
            driver.find_element_by_xpath(CreateManageTaskPage.manage_subtask_button).click()

            task_todo_id = driver.find_element_by_xpath(CreateManageSubTaskPage.todo_task_id)
            if task_todo_id.is_enabled():
                return "fail"
            else:
                return "pass"