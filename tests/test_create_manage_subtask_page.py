from selenium import webdriver
from pageobject.create_manage_task_page import CreateManageTaskPage
from pageobject.create_manage_subtask_page import CreateManageSubTaskPage
from webutility.stringutils import StringUtils
from webutility.fileutils import GeneralUtility

import unittest
import time

class TestCreateAndManageSubTask(unittest.TestCase):
    """
    1. Once create task, user should see 'Manage Subtasks' button
    test case name: test_001_see_manage_subtask_button

    2. The 'Manage Subtasks' button should have a number
    test case name: test_002_manage_subtask_default_number
                    test_003_manage_subtask_increase_number_by_add_subtask

    3. Created Subtask should append and display on the bottom of dialog
    test case coverred by create manage subtask, need verify on the bottom of dialog

    4. SubTask description and SubTask due date both are required
    test case name: test_004_create_invalid_subtask_with_empty_subtask_desc
                    test_005_create_invalid_subtask_with_empty_subtask_due_date

    5. Task id and task description only ready
    test case name: test_006_verify_task_todo_id_read_only
                    test_007_verify_task_todo_desc_read_only

    6. Click 'Manage Subtasks' button to create a sub task, need verify valid subtask desc and subtask due date
    test case name:

    """
    gu = GeneralUtility()

    def setUp(self):
        self.dr = webdriver.Chrome()

    def test_001_see_manage_subtask_button(self):
        driver = self.dr
        new_task = CreateManageTaskPage()
        if new_task.createTaskName(driver=driver, task_name_str="Create main task" + self.gu.alpha_generator(size=3, utf8=False)) == "pass":
            if driver.find_element_by_xpath(CreateManageTaskPage.manage_subtask_button).is_displayed():
                return "pass"
            else:
                return "fail"


    def test_002_manage_subtask_default_number(self):
        driver = self.dr
        new_task = CreateManageTaskPage()
        gu = StringUtils()

        if new_task.createTaskName(driver=driver, task_name_str="Create main task" + self.gu.alpha_generator(size=3, utf8=False)) == "pass":
            subTask_str = driver.find_element_by_xpath(CreateManageTaskPage.manage_subtask_button).text
            subTask_number = gu.getNumber(subTaskStr=subTask_str)

            if (subTask_number == 0):
                return "pass"
            else:
                return "fail"


    def test_003_manage_subtask_increase_number_by_add_subtask(self):
        driver = self.dr
        new_SubTask = CreateManageSubTaskPage()
        gu = StringUtils()

        if new_SubTask.inputValidSubTask(driver=driver)== "pass":
            subTask_str = driver.find_element_by_xpath(CreateManageTaskPage.manage_subtask_button).text
            subTask_number = gu.getNumber(subTaskStr=subTask_str)

            if (subTask_number == 1):
                return "pass"
            else:
                return "fail"


    def test_004_create_invalid_subtask_with_empty_subtask_desc(self):
        driver = self.dr
        new_SubTask = CreateManageSubTaskPage()

        if new_SubTask.emptySubTaskDescription(driver=driver) == "pass":
            self.assertFalse(True)
            print "subTask desc should be required"
        else:
            self.assertTrue(True)


    def test_005_create_invalid_subtask_with_empty_subtask_due_date(self):
        driver = self.dr
        new_SubTask = CreateManageSubTaskPage()

        if new_SubTask.emptySubTaskDueDate(driver=driver) == "pass":
            return "fail"
        else:
            self.assertTrue(True)


    def test_006_verify_task_todo_id_read_only(self):
        driver = self.dr
        new_SubTask = CreateManageSubTaskPage()

        if new_SubTask.validateTaskToDoIdReadOnly(driver=driver) == "pass":
            return "fail"
        else:
            self.assertTrue(True)


    def test_007_verify_task_todo_desc_read_only(self):
        driver = self.dr
        new_SubTask = CreateManageSubTaskPage()

        if new_SubTask.validateTaskToDoIdReadOnly(driver=driver) == "pass":
            return "fail"
        else:
            self.assertTrue(True)


    def test_008_create_valid_subTask_desc(self):
        subTask_desc = self.gu.alpha_generator(size=15, utf8=False)
        new_SubTask = CreateManageSubTaskPage()
        new_SubTask.validateSubTaskDesc(self.dr, input_task_name=subTask_desc)

    def test_006_create_invalid_empty_subTask_desc(self):
        subTask_desc = ""
        new_SubTask = CreateManageSubTaskPage()
        new_SubTask.validateSubTaskDesc(self.dr, input_task_name=subTask_desc)

    def test_007_create_invalid_subTask_desc_length_more_than_250_chars(self):
        subTask_desc = self.gu.alpha_generator(size=260, utf8=False)
        new_SubTask = CreateManageSubTaskPage()
        new_SubTask.validateSubTaskDesc(self.dr, input_task_name=subTask_desc)

    def test_008_create_invalid_subTask_desc_only_numeric_value(self):
        subTask_desc = self.gu.num_size_generator(size=5, utf8=False)
        new_SubTask = CreateManageSubTaskPage()
        new_SubTask.validateSubTaskDesc(self.dr, input_task_name=subTask_desc)

    def test_009_create_invalid_subTask_desc_only_three_white_spaces(self):
        subTask_desc = "   "
        new_SubTask = CreateManageSubTaskPage()
        new_SubTask.validateSubTaskDesc(self.dr, input_task_name=subTask_desc)

    def test_010_create_invalid_subTask_desc_only_specail_chars(self):
        subTask_desc = "!!!"
        new_SubTask = CreateManageSubTaskPage()
        new_SubTask.validateSubTaskDesc(self.dr, input_task_name=subTask_desc)

    def test_011_create_invalid_subTask_desc_length_more_than_250_Chinese_chars(self):
        subTask_desc = self.gu.alpha_generator(size=260, utf8=True)
        new_SubTask = CreateManageSubTaskPage()
        new_SubTask.validateSubTaskDesc(self.dr, input_task_name=subTask_desc)

    def test_012_create_invalid_subTask_due_date_days_more_than_31(self):
        subTask_due_date = "32/09/2016"
        new_SubTask = CreateManageSubTaskPage()
        new_SubTask.validateSubTaskDueDate(self.dr, input_due_date=subTask_due_date)

    def test_013_create_invalid_subTask_due_date_days_less_than_1(self):
        subTask_due_date = "-1/09/2016"
        new_SubTask = CreateManageSubTaskPage()
        new_SubTask.validateSubTaskDueDate(self.dr, input_due_date=subTask_due_date)

    def test_012_create_invalid_subTask_due_date_month_more_than_12(self):
        subTask_due_date = "12/13/2016"
        new_SubTask = CreateManageSubTaskPage()
        new_SubTask.validateSubTaskDueDate(self.dr, input_due_date=subTask_due_date)

    def test_013_create_invalid_subTask_due_date_month_less_than_1(self):
        subTask_due_date = "12/-1/2016"
        new_SubTask = CreateManageSubTaskPage()
        new_SubTask.validateSubTaskDueDate(self.dr, input_due_date=subTask_due_date)

    def test_014_create_invalid_subTask_due_date_year_less_than_2000(self):
        subTask_due_date = "22/09/1900"
        new_SubTask = CreateManageSubTaskPage()
        new_SubTask.validateSubTaskDueDate(self.dr, input_due_date=subTask_due_date)

    def test_015_create_invalid_subTask_due_date_format_is_not_correct(self):
        subTask_due_date = "12-11-2015"
        new_SubTask = CreateManageSubTaskPage()
        new_SubTask.validateSubTaskDueDate(self.dr, input_due_date=subTask_due_date)
