# coding=utf-8
from selenium.webdriver.common.by import By
from pageobject.login_register_page import LoginRegisterPage
from webutility.stringutils import StringUtils
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class CreateManageTaskPage:
    """
    1. Task page elements, such as form, button, option can be selectable
    2. Task page actions, such as verifyValidTaskName, createTaskName
    """
    header_my_tasks = "//div//a[text()='My Tasks']"
    my_task_button = "//center/a[text()='My Tasks']"
    my_task_todo_alert = "//h1"
    welcome_alert = "//ul[@class='nav navbar-nav navbar-right']/li[1]/a"
    new_task_input_form = "//input[@id='new_task']"
    add_new_task_button = "//div[@class='input-group']/span"
    new_task_latest_list = "//tr[@class='ng-scope'][1]/td[2]"
    manage_subtask_button = "//tr[@class='ng-scope'][1]/td[4]/button"


    def verifyValidTaskName(self, driver, input_task_name):

        test_login = LoginRegisterPage()
        sign_in_msg = test_login.login(driver)
        su = StringUtils()
        if sign_in_msg == "Signed in successfully.":
                driver.find_element_by_xpath(CreateManageTaskPage.header_my_tasks).click()

                task_name_form = driver.find_element_by_xpath(CreateManageTaskPage.new_task_input_form)
                add_task_button = driver.find_element_by_xpath(CreateManageTaskPage.add_new_task_button)
                task_name_form.send_keys(input_task_name)
                add_task_button.click()

                if len(input_task_name) < 3:
                    return "fail"
                elif len(input_task_name) > 250:
                    return "fail"
                elif su.is_number(s=input_task_name):
                    return "fail"
                elif su.is_specail_chars(s=input_task_name):
                    return "fail"
                elif su.is_white_spaces(s=input_task_name):
                    return "fail"
                else:
                    return "pass"


    def createTaskName(self, driver, task_name_str):

        test_login = LoginRegisterPage()
        sign_in_msg = test_login.login(driver)
        if sign_in_msg == "Signed in successfully.":
                driver.find_element_by_xpath(CreateManageTaskPage.header_my_tasks).click()

                task_name_form = driver.find_element_by_xpath(CreateManageTaskPage.new_task_input_form)
                add_task_button = driver.find_element_by_xpath(CreateManageTaskPage.add_new_task_button)
                task_name_form.send_keys(task_name_str)
                add_task_button.click()

                latest_task_str = driver.find_element_by_xpath(CreateManageTaskPage.new_task_latest_list).text
                if latest_task_str == task_name_str:
                    return "pass"
                else:
                    return "fail"

    def viewMyTaskLink(self, driver):
        test_login = LoginRegisterPage()
        sign_in_msg = test_login.login(driver)

        if sign_in_msg == "Signed in successfully.":
            my_task = driver.find_element_by_xpath(CreateManageTaskPage.my_task_button)
            if my_task.is_displayed():
                return "pass"
            else:
                return "fail"

    def viewMyTasListMsg(self, driver):
        test_login = LoginRegisterPage()
        sign_in_msg = test_login.login(driver)
        su = StringUtils()

        if sign_in_msg == "Signed in successfully.":
            driver.find_element_by_xpath(CreateManageTaskPage.my_task_button).click()
            for i in range(4):
                try:
                    welcome = WebDriverWait(driver, 10).until(
                        EC.presence_of_element_located((By.XPATH, driver.find_element_by_xpath(CreateManageTaskPage.welcome_alert)))

                    )
                    welcome_str = welcome.text
                    if su.welcomeUser(s=welcome_str) == "Alan":
                        return "pass"
                    else:
                        return "fail"
                    break
                finally:
                    return "fail"

