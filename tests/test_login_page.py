from selenium import webdriver
from pageobject.login_register_page import LoginRegisterPage

import unittest

class TestLoginPage(unittest.TestCase):
    """
    1. Login by register user
    test case name: test_001_login_by_register_user
    """

    def setUp(self):
        self.dr = webdriver.Chrome()

    def test_001_login_by_register_user(self):
        login_test = LoginRegisterPage()
        if login_test.login(self.dr) == "Signed in successfully.":
            return "pass"
        else:
            return "fail"

if __name__ == '__main__':
    unittest.main()

