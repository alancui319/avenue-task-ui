from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from pageobject.login_register_page import LoginRegisterPage
from pageobject.create_manage_task_page import CreateManageTaskPage
from webutility.fileutils import GeneralUtility

import unittest

class TestCreateAndManageTask(unittest.TestCase):
    """
    1. Front-end: User always see 'My Tasks' link
    test case name: test_001_see_my_task_by_login_registered_user

    2. Front-end: User is able to create a new task
    test case name: test_002_create_new_task_by_hit_enter_key
                    test_003_create_new_task_by_hit_add_button

    3. Front-end: User will see message saying task list belongs to logged-in user name
    test case name: test_004_see_my_task_by_login_registered_user

    4. Front-end: Task name requires >= three chars and <=250 chars
    test case name: test_005_create_valid_task_name
                    test_006_create_invalid_task_name_length_less_than_3_chars
                    test_007_create_invalid_task_name_length_more_than_250_chars
                    test_008_create_invalid_task_name_only_numeric_value
                    test_009_create_invalid_task_name_only_three_white_spaces
                    test_010_create_invalid_task_name_only_specail_chars
                    test_011_create_invalid_task_name_length_more_than_250_Chinese_chars

    5. Front-end: New task always on the top of task list
    test case coverred by create_new_task then verify the top of task list, such as test_003_create_new_task_by_hit_enter_key

    """
    gu = GeneralUtility()

    def setUp(self):
        self.dr = webdriver.Chrome()

    def test_001_see_my_task_by_login_registered_user(self):
        new_task = CreateManageTaskPage()
        if new_task.viewMyTaskLink(self.dr) == "pass":
            self.assertTrue(True)
        else:
            self.assertFalse(True)

    def test_002_create_new_task_by_hit_enter_key(self):
        driver = self.dr
        login_test = LoginRegisterPage()
        if login_test.login(driver) == "Signed in successfully.":
            driver.find_element_by_xpath(CreateManageTaskPage.header_my_tasks).click()

            task_name = driver.find_element_by_xpath(CreateManageTaskPage.new_task_input_form)
            task_name_str = "Create first login test case"
            task_name.send_keys(task_name_str, Keys.RETURN)

            latest_task_str = driver.find_element_by_xpath(CreateManageTaskPage.new_task_latest_list).text
            if latest_task_str == task_name_str:
                return "pass"
            else:
                return "fail"

    def test_003_create_new_task_by_hit_add_button(self):
        driver = self.dr
        login_test = LoginRegisterPage()
        if login_test.login(driver) == "Signed in successfully.":
            driver.find_element_by_xpath(CreateManageTaskPage.my_task_button).click()

            task_name = driver.find_element_by_xpath(CreateManageTaskPage.new_task_input_form)
            add_button = driver.find_element_by_xpath(CreateManageTaskPage.add_new_task_button)

            task_name_str = "Create second login test case"
            task_name.send_keys(task_name_str)
            add_button.click()

            latest_task_str = driver.find_element_by_xpath(CreateManageTaskPage.new_task_latest_list).text
            if latest_task_str == task_name_str:
                return "pass"
            else:
                return "fail"

    def test_004_see_my_task_name_belongs_to_registered_user(self):
        new_task = CreateManageTaskPage()
        if new_task.viewMyTasListMsg(self.dr) == "pass":
            self.assertTrue(True)
        else:
            self.assertFalse(False)

    def test_005_create_valid_task_name(self):
        task_name = self.gu.alpha_generator(size=5, utf8=False)
        test_task_name = CreateManageTaskPage()
        test_task_name.verifyValidTaskName(self.dr, input_task_name=task_name)

    def test_006_create_invalid_task_name_length_less_than_3_chars(self):
        task_name = self.gu.alpha_generator(size=2, utf8=False)
        test_task_name = CreateManageTaskPage()
        test_task_name.verifyValidTaskName(self.dr, input_task_name=task_name)

    def test_007_create_invalid_task_name_length_more_than_250_chars(self):
        task_name = self.gu.alpha_generator(size=260, utf8=False)
        test_task_name = CreateManageTaskPage()
        test_task_name.verifyValidTaskName(self.dr, input_task_name=task_name)

    def test_008_create_invalid_task_name_only_numeric_value(self):
        task_name = self.gu.num_size_generator(size=5, utf8=False)
        test_task_name = CreateManageTaskPage()
        test_task_name.verifyValidTaskName(self.dr, input_task_name=task_name)

    def test_009_create_invalid_task_name_only_three_white_spaces(self):
        task_name = "   "
        create_task_name = CreateManageTaskPage()
        create_task_name.verifyValidTaskName(self.dr, input_task_name=task_name)

    def test_010_create_invalid_task_name_only_specail_chars(self):
        task_name = "!!!"
        create_task_name = CreateManageTaskPage()
        create_task_name.verifyValidTaskName(self.dr, input_task_name=task_name)

    def test_011_create_invalid_task_name_length_more_than_250_Chinese_chars(self):
        task_name = self.gu.alpha_generator(size=260, utf8=True)
        test_task_name = CreateManageTaskPage()
        test_task_name.verifyValidTaskName(self.dr, input_task_name=task_name)